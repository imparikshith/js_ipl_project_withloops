const csvFilePath = "../data/deliveries.csv";
const csv = require("csvtojson");
const fs = require("node:fs");
csv()
  .fromFile(csvFilePath)
  .then((jsonObj) => {
    // console.log(jsonObj);
    const bowlersInSuperOver = {};
    for (let delivery of jsonObj) {
      if (parseInt(delivery.is_super_over)) {
        if (bowlersInSuperOver[delivery.bowler]) {
          bowlersInSuperOver[delivery.bowler].runs += parseInt(
            delivery.total_runs
          );
          if (
            !(parseInt(delivery.noball_runs) || parseInt(delivery.wide_runs))
          ) {
            bowlersInSuperOver[delivery.bowler].balls++;
          }
        } else {
          bowlersInSuperOver[delivery.bowler] = {};
          bowlersInSuperOver[delivery.bowler].runs = parseInt(
            delivery.total_runs
          );
          if (
            !(parseInt(delivery.noball_runs) || parseInt(delivery.wide_runs))
          ) {
            bowlersInSuperOver[delivery.bowler].balls = 1;
          } else {
            bowlersInSuperOver[delivery.bowler].balls = 0;
          }
        }
      }
    }
    for (let bowler in bowlersInSuperOver) {
      bowlersInSuperOver[bowler] =
        (bowlersInSuperOver[bowler].runs / bowlersInSuperOver[bowler].balls) *
        6;
    }
    const entries = Object.entries(bowlersInSuperOver);
    entries.sort((a, b) => a[1] - b[1]);
    const mostEconomicalSuperOverBowlers = Object.fromEntries(entries);
    const economicalBowlerInSuperOver = {};
    for (let bowler in mostEconomicalSuperOverBowlers) {
      economicalBowlerInSuperOver[bowler] =
        mostEconomicalSuperOverBowlers[bowler];
      break;
    }
    fs.writeFileSync(
      "../public/output/bowlerWithBestEconomyInSuperOver.json",
      JSON.stringify(economicalBowlerInSuperOver)
    );
  });