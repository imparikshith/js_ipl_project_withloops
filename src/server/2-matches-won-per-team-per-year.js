const csvFilePath = "../data/matches.csv";
const csv = require("csvtojson");
const fs = require("node:fs");
csv()
  .fromFile(csvFilePath)
  .then((jsonObj) => {
    // console.log(jsonObj);
    /**
     * [
     * 	{a:"1", b:"2", c:"3"},
     * 	{a:"4", b:"5". c:"6"}
     * ]
     */
    const matchesWonPerTeamPerYear = {};
    for (let match of jsonObj) {
      if (matchesWonPerTeamPerYear[match.winner]) {
        if (matchesWonPerTeamPerYear[match.winner][match.season]) {
          matchesWonPerTeamPerYear[match.winner][match.season]++;
        } else {
          matchesWonPerTeamPerYear[match.winner][match.season] = 1;
        }
      } else {
        matchesWonPerTeamPerYear[match.winner] = {};
        matchesWonPerTeamPerYear[match.winner][match.season] = 1;
      }
    }
    fs.writeFileSync(
      "../public/output/matchesWonPerTeamPerYear.json",
      JSON.stringify(matchesWonPerTeamPerYear)
    );
  });