const csvFilePath = "../data/matches.csv";
const csv = require("csvtojson");
const fs = require("node:fs");
function sortObj(obj) {
  const entries = Object.entries(obj);
  entries.sort((a, b) => b[1] - a[1]);
  return Object.fromEntries(entries);
}
csv()
  .fromFile(csvFilePath)
  .then((jsonObj) => {
    // console.log(jsonObj);
    const PoMAwards = {};
    for (let match of jsonObj) {
      if (PoMAwards[match.season]) {
        if (PoMAwards[match.season][match.player_of_match]) {
          PoMAwards[match.season][match.player_of_match]++;
        } else {
          PoMAwards[match.season][match.player_of_match] = 1;
        }
      } else {
        PoMAwards[match.season] = {};
        PoMAwards[match.season][match.player_of_match] = 1;
      }
    }
    // console.log(PoMAwards);
    const sortedPoMAwards = {};
    for (let season in PoMAwards) {
      sortedPoMAwards[season] = {};
      sortedPoMAwards[season] = sortObj(PoMAwards[season]);
    }
    console.log(sortedPoMAwards);
    const playerWithMostPoMAwardsPerSeason = {};
    for (let season in sortedPoMAwards) {
      playerWithMostPoMAwardsPerSeason[season] = {};
      for (let player in sortedPoMAwards[season]) {
        playerWithMostPoMAwardsPerSeason[season][player] =
          sortedPoMAwards[season][player];
        break;
      }
    }
    fs.writeFileSync(
      "../public/output/playerWithMostPoMAwardsPerSeason.json",
      JSON.stringify(playerWithMostPoMAwardsPerSeason)
    );
  });