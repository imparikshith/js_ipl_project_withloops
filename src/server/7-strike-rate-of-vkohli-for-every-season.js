const csvFilePathDeliveries = "../data/deliveries.csv";
const csvFilePathMatches = "../data/matches.csv";
const csv = require("csvtojson");
const fs = require("node:fs");
csv()
  .fromFile(csvFilePathDeliveries)
  .then((jsonObjDeliveries) => {
    // console.log(jsonObjDeliveries);

    csv()
      .fromFile(csvFilePathMatches)
      .then((jsonObjMatches) => {
        // console.log(jsonObjMatches);
        const matchIdsForEverySeason = {};
        for (let match of jsonObjMatches) {
          if (matchIdsForEverySeason[match.season]) {
            matchIdsForEverySeason[match.season].push(match.id);
          } else {
            matchIdsForEverySeason[match.season] = [];
            matchIdsForEverySeason[match.season].push(match.id);
          }
        }
        // console.log(matchIdsForEverySeason);
        const runsAndBallsEverySeason = {};
        for (let delivery of jsonObjDeliveries) {
          if (delivery.batsman == "V Kohli") {
            for (let year in matchIdsForEverySeason) {
              if (matchIdsForEverySeason[year].includes(delivery.match_id)) {
                if (runsAndBallsEverySeason[year]) {
                  runsAndBallsEverySeason[year].runs += parseInt(
                    delivery.batsman_runs
                  );
                  if (!parseInt(delivery.wide_runs)) {
                    runsAndBallsEverySeason[year].balls++;
                  }
                } else {
                  runsAndBallsEverySeason[year] = {};
                  runsAndBallsEverySeason[year].runs = parseInt(
                    delivery.batsman_runs
                  );
                  if (!parseInt(delivery.wide_runs)) {
                    runsAndBallsEverySeason[year].balls = 1;
                  } else {
                    runsAndBallsEverySeason[year].balls = 0;
                  }
                }
              }
            }
          }
        }
        console.log(runsAndBallsEverySeason);
        const strikeRateEverySeason = {};
        for (let season in runsAndBallsEverySeason) {
          strikeRateEverySeason[season] =
            (runsAndBallsEverySeason[season].runs /
              runsAndBallsEverySeason[season].balls) *
            100;
        }

        fs.writeFileSync(
          "../public/output/strikeRateOfVKohliEverySeason.json",
          JSON.stringify(strikeRateEverySeason)
        );
      });
  });