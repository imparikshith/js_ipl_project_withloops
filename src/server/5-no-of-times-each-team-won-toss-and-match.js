const csvFilePath = "../data/matches.csv";
const csv = require("csvtojson");
const fs = require("node:fs");
csv()
  .fromFile(csvFilePath)
  .then((jsonObj) => {
    // console.log(jsonObj);
    const noOfTimesEachTeamWonTossAndMatch = {};
    for (let match of jsonObj) {
      if (match.toss_winner == match.winner) {
        if (noOfTimesEachTeamWonTossAndMatch[match.winner]) {
          noOfTimesEachTeamWonTossAndMatch[match.winner]++;
        } else {
          noOfTimesEachTeamWonTossAndMatch[match.winner] = 1;
        }
      }
    }
    fs.writeFileSync(
      "../public/output/noOfTimesEachTeamWonTossAndMatch.json",
      JSON.stringify(noOfTimesEachTeamWonTossAndMatch)
    );
  });