const csvFilePath = "../data/matches.csv";
const csv = require("csvtojson");
const fs = require("node:fs");
csv()
  .fromFile(csvFilePath)
  .then((jsonObj) => {
    // console.log(jsonObj);
    /**
     * [
     * 	{a:"1", b:"2", c:"3"},
     * 	{a:"4", b:"5". c:"6"}
     * ]
     */
    const matchesPerYear = {};
    for (let match of jsonObj) {
      if (matchesPerYear[match.season]) {
        matchesPerYear[match.season]++;
      } else {
        matchesPerYear[match.season] = 1;
      }
    }
    fs.writeFileSync(
      "../public/output/matchesPerYear.json",
      JSON.stringify(matchesPerYear)
    );
  });