const csvFilePath = "../data/deliveries.csv";
const csv = require("csvtojson");
const fs = require("node:fs");
csv()
  .fromFile(csvFilePath)
  .then((jsonObj) => {
    // console.log(jsonObj);
    const dismissalsByBowlers = {};
    for (let delivery of jsonObj) {
      if (delivery.batsman == "V Kohli") {
        if (
          delivery.player_dismissed &&
          delivery.dismissal_kind !== "run out"
        ) {
          if (dismissalsByBowlers[delivery.bowler]) {
            dismissalsByBowlers[delivery.bowler]++;
          } else {
            dismissalsByBowlers[delivery.bowler] = 1;
          }
        }
      }
    }
    const entries = Object.entries(dismissalsByBowlers);
    entries.sort((a, b) => b[1] - a[1]);
    const mostDismissalsByBowler = Object.fromEntries(entries);
    const mostDismissals = {};
    for (let bowler in mostDismissalsByBowler) {
      mostDismissals[bowler] = mostDismissalsByBowler[bowler];
      break;
    }
    fs.writeFileSync(
      "../public/output/mostTimesAPlayerDismissedByAnotherPlayer.json",
      JSON.stringify(mostDismissals)
    );
  });